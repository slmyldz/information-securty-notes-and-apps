Pcap-parser 0.4.4

=>Install
pip install pcap-parser

=>Parse Pcap File
# only output the requested URL and response status
parse_pcap test.pcap

# output http req/resp headers
parse_pcap -v test.pcap

# output http req/resp headers and body which belong to text type
parse_pcap -vv test.pcap

# output http req/resp headers and body
parse_pcap -vvv test.pcap

# display and attempt to do url decoding and formatting json output
parse_pcap -vvb test.pcap

=>Group
# Use -g to group http request/response:
parse_pcap -g test.pcap

=>Filter
#You can use the -p/-i to specify the ip/port of source and destination, will only display http data meets the specified conditions:
parse_pcap -p55419 -vv test.pcap
parse_pcap -i192.168.109.91 -vv test.pcap

#Use -d to specify the http domain, only display http req/resp with the domain:
parse_pcap -dwww.baidu.com -vv test.pcap

#Use -u to specify the http uri pattern, only dispay http req/resp which url contains the url pattern:
parse_pcap -u/api/update -vv test.pcap

#Use -e can forced the encoding http body used:
parse_pcap -i192.168.109.91 -p80 -vv -eutf-8 test.pcap